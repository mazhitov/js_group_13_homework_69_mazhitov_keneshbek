import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Client } from './client.model';
import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  clientsIsFetching = new Subject<boolean>();
  clientsIsUploading = new Subject<boolean>();
  clientsIsRemoving = new Subject<boolean>();
  clientsChange = new Subject<Client[]>();

  private clients: Client[] = [];

  sizes = ['S', 'M', 'L', 'XL'];
  skills = ['Javascript', 'React', 'Angular', 'Vue'];
  levelOfSkills = ['Beginner', 'Intermediate', 'Advanced', 'Expert'];

  constructor(private http: HttpClient) {}

  fetchClients() {
    this.clientsIsFetching.next(true);
    this.http.get<{ [id: string]: Client }>('https://project-server-788da-default-rtdb.firebaseio.com/clients.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const client = result[id];
          return new Client(id, client.firstName, client.lastName, client.patronymic, client.phone,
            client.work, client.gender, client.size, client.comment, client.skills);
        });
      }))
      .subscribe(clients => {
        this.clients = clients;
        this.clientsChange.next(this.clients.slice());
        this.clientsIsFetching.next(false);
      }, () => {
        this.clientsIsFetching.next(false);
      });
  }

  fetchClient(id:string) {
    return this.http.get<Client | null>(`https://project-server-788da-default-rtdb.firebaseio.com/clients/${id}.json`).pipe(
      map(result => {
        if (!result) {
          return null;
        }
        return new Client(id, result.firstName, result.lastName, result.patronymic, result.phone,
          result.work, result.gender, result.size, result.comment, result.skills);
      }),
    );
  }

  registrate(client: Client) {
    const body ={
      firstName: client.firstName,
      lastName: client.lastName,
      patronymic: client.patronymic,
      phone: client.phone,
      work: client.work,
      gender: client.gender,
      size: client.size,
      comment: client.comment,
      skills: client.skills,
    };
    console.log(body);
    return this.http.post('https://project-server-788da-default-rtdb.firebaseio.com/clients.json', body);
  }

  editClient(client: Client) {
    this.clientsIsUploading.next(true);
    const body = {
      firstName: client.firstName,
      lastName: client.lastName,
      patronymic: client.patronymic,
      phone: client.phone,
      work: client.work,
      gender: client.gender,
      size: client.size,
      comment: client.comment,
      skills: client.skills,
    };

    return this.http.put(`https://project-server-788da-default-rtdb.firebaseio.com/clients/${client.id}.json`,
      body).pipe(
      tap(() => {
        this.clientsIsUploading.next(false);
      }, () => {
        this.clientsIsUploading.next(false);
      })
    );
  }

  removeClient(id:string) {
    this.clientsIsRemoving.next(true);
    return this.http.delete(`https://project-server-788da-default-rtdb.firebaseio.com/clients/${id}.json`).pipe(
      tap(() => {
        this.clientsIsRemoving.next(false);
      }, () => {
        this.clientsIsRemoving.next(false);
      })
    );
  }

}
