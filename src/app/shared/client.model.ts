export class Client{
  constructor(
    public id:string,
    public firstName:string,
    public lastName:string,
    public patronymic:string,
    public phone:string,
    public work:string,
    public gender:string,
    public size:string,
    public comment:string,
    public skills = [{skill: '', level: ''}],
  ){}
}
