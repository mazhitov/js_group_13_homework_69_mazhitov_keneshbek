import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { Client } from './client.model';
import { ClientService } from './client.service';
import { EMPTY, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientResolverService implements Resolve<Client> {

  constructor(private clientService: ClientService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Client> | Observable<never> {
    const clientId = <string>route.params['id'];

    return this.clientService.fetchClient(clientId).pipe(mergeMap(client => {
      if (client) {
        return of(client);
      }
      void this.router.navigate(['/clients']);
      return EMPTY;
    }));
  }
}
