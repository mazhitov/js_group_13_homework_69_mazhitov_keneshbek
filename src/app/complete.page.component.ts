import { Component } from '@angular/core';

@Component({
  selector: 'app-complete',
  template: `<h1>you have registered successfully</h1>`,
  styles: [`
    h1 {
      color: red;
    }
  `]
})
export class CompletePageComponent {}
