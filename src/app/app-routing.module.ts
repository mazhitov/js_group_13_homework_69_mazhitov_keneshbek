import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompletePageComponent } from './complete.page.component';
import { NotFoundComponent } from './not-found.component';
import { EditAddComponent } from './clients/edit-add/edit-add.component';
import { ClientsComponent } from './clients/clients.component';
import { EmptyClientComponent } from './clients/empty-client.component';
import { ClientComponent } from './clients/client/client.component';
import { ClientResolverService } from './shared/client-resolver.service';

const routes: Routes = [
  {path: '', component: EditAddComponent},
  {
    path: 'clients', component: ClientsComponent, children: [
      {path: '', component: EmptyClientComponent},
      {path: ':id', component: ClientComponent, resolve: {
        client: ClientResolverService
        }
      },
      {path: ':id/edit', component: EditAddComponent, resolve: {
          client: ClientResolverService
        }
      },
    ]
  },
  {path: 'complete', component: CompletePageComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
