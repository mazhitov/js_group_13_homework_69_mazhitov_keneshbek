import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CompletePageComponent } from './complete.page.component';
import { NotFoundComponent } from './not-found.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ValidatePhoneDirective } from './directives/validate-phone.directive';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ClientsComponent } from './clients/clients.component';
import { EditAddComponent } from './clients/edit-add/edit-add.component';
import { ClientComponent } from './clients/client/client.component';
import { EmptyClientComponent } from './clients/empty-client.component';

@NgModule({
  declarations: [
    AppComponent,
    CompletePageComponent,
    NotFoundComponent,
    ValidatePhoneDirective,
    ToolbarComponent,
    ClientsComponent,
    EditAddComponent,
    ClientComponent,
    EmptyClientComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
