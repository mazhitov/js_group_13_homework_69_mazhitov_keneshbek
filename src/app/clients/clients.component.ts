import { Component, OnDestroy, OnInit } from '@angular/core';
import { ClientService } from '../shared/client.service';
import { Client } from '../shared/client.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit, OnDestroy {
  clients: Client[] = [];
  clientsChangeSubscription!: Subscription;
  clientsIsFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private clientsService: ClientService) { }

  ngOnInit(): void {
    this.clientsChangeSubscription = this.clientsService.clientsChange.subscribe((clients: Client[]) => {
      this.clients = clients;
    });

    this.clientsIsFetchingSubscription = this.clientsService.clientsIsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.clientsService.fetchClients();
  }

  ngOnDestroy() {
    this.clientsChangeSubscription.unsubscribe();
    this.clientsIsFetchingSubscription.unsubscribe();
  }

  getFullName(client: Client) {
    return `${client.firstName} ${client.lastName} ${client.patronymic}`;
  }
}
