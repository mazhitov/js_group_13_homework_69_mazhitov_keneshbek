import { Component, OnDestroy, OnInit } from '@angular/core';
import { Client } from '../../shared/client.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { ClientService } from '../../shared/client.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit, OnDestroy {
  client!: Client;
  isRemoving = false;
  clientRemovingSubscription!: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private clientService: ClientService
  ) {
  }

  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
      this.client = <Client>data.client;
    });

    this.clientRemovingSubscription = this.clientService.clientsIsRemoving.subscribe((isRemoving: boolean) => {
      this.isRemoving = isRemoving;
    });
  }


  ngOnDestroy() {
    this.clientRemovingSubscription.unsubscribe();
  }

  onRemove() {
    this.clientService.removeClient(this.client.id).subscribe(() => {
      this.clientService.fetchClients();
      void this.router.navigate(['..'], {relativeTo: this.route});
    });
  }
}
