import { Component } from '@angular/core';

@Component({
  selector: 'app-empty-client',
  template: `
    <h4>Client information</h4>
    <p>No client is selected</p>
  `
})
export class EmptyClientComponent {}
