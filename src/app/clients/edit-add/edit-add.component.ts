import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ClientService } from '../../shared/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Client } from '../../shared/client.model';
import { Subscription } from 'rxjs';
import { phoneValidator } from '../../directives/validate-phone.directive';

@Component({
  selector: 'app-edit-add',
  templateUrl: './edit-add.component.html',
  styleUrls: ['./edit-add.component.css']
})
export class EditAddComponent implements OnInit, OnDestroy {
  clientForm!: FormGroup;
  sizes: string[] = [];
  skills: string[] = [];
  levels: string[] = [];
  commentMaxLength = 300;
  isEdit = false;
  editedId = '';
  phoneLength = 15;
  isUploading = false;
  clientUploadingSubscription!: Subscription;

  constructor(private clientService: ClientService,
              private router: Router,
              private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.sizes = this.clientService.sizes;
    this.skills = this.clientService.skills;
    this.levels = this.clientService.levelOfSkills;

    this.clientUploadingSubscription = this.clientService.clientsIsUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });

    this.route.data.subscribe(data => {
      const client = <Client | null>data.client;

      if (client) {
        this.isEdit = true;
        this.editedId = client.id;
        this.setFormValue({
          firstName: client.firstName,
          lastName: client.lastName,
          patronymic: client.patronymic,
          phone: client.phone,
          work: client.work,
          gender: client.gender,
          size: client.size,
          comment: client.comment,
          skills: client.skills,
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          firstName: '',
          lastName: '',
          patronymic: '',
          phone: '',
          work: '',
          gender: '',
          size: '',
          comment: '',
          skills: [],
        });
      }
    });

    this.clientForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      patronymic: new FormControl('', Validators.required),
      phone: new FormControl('', [
        Validators.required,
        phoneValidator
      ]),
      work: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      comment: new FormControl('', [Validators.required, Validators.maxLength(this.commentMaxLength)]),
      skills: new FormArray([]),
    });
  }

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      const skills = value.skills;
      this.clientForm.patchValue(value);
      const newFormArray = new FormArray([]);
      skills.forEach((skill: {skill: '', level: ''}) => {
        const  newFormGroup = new FormGroup({
          skill: new FormControl(skill.skill, Validators.required ),
          level: new FormControl(skill.level, Validators.required),
        })
        newFormArray.push(newFormGroup);
      });
      this.clientForm.setControl('skills', newFormArray);
    });
  }


  Registrate() {
    const id = this.editedId || '';
    const newClient = new Client(
      id,
      this.clientForm.get('firstName')?.value,
      this.clientForm.get('lastName')?.value,
      this.clientForm.get('patronymic')?.value,
      this.clientForm.get('phone')?.value,
      this.clientForm.get('work')?.value,
      this.clientForm.get('gender')?.value,
      this.clientForm.get('size')?.value,
      this.clientForm.get('comment')?.value,
      this.clientForm.get('skills')?.value
    );

    if (this.isEdit) {
      this.clientService.editClient(newClient).subscribe(() => {
        this.clientService.fetchClients();
        void this.router.navigate(['..'], {relativeTo: this.route});
      });
    } else {
      this.clientService.registrate(newClient)
        .subscribe(() => {
          void this.router.navigate(['/complete'], {relativeTo: this.route});
        });
    }
  }

  fieldHasError(fieldName: string, errorType:string) {
    const field = this.clientForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  groupFieldHasError(i:number, fieldName:string, errorType:string) {
    const fieldArray = <FormArray>this.clientForm.get('skills');
    const fieldGroup = <FormGroup>fieldArray.controls[i];
    const field = fieldGroup.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }
  ngOnDestroy() {
    this.clientUploadingSubscription.unsubscribe();
  }

  getCurrentLength() {
    return this.clientForm.get('comment')?.value.length;
  }

  AddSkill() {
    const skills = <FormArray>this.clientForm.get('skills');
    const skillGroup = new FormGroup({
      skill: new FormControl('', Validators.required),
      level: new FormControl('', Validators.required),
    });
    skills.push(skillGroup);
  }

  getControls() {
    const skills = <FormArray>this.clientForm.get('skills');
    return skills.controls;
  }
}
