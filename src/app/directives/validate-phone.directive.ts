import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

export const phoneValidator = (control: AbstractControl): ValidationErrors | null => {
    const identityPhone = /^\+\996.\s?\d{3}.\s?\d{6}/.test(control.value);
    if (identityPhone){
      return null;
    }
    return {format: true};

};

@Directive({
  selector: '[appPhone]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidatePhoneDirective,
    multi: true
  }]
})
export class ValidatePhoneDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return phoneValidator(control);
  }
}
